package com.experis.moviecharacters.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a Character entity.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="full_name", length=80)
    @JsonProperty("full_name")
    private String fullName;

    @Column(name = "gender", length=10)
    private String gender;

    @Column(length=35)
    private String alias;

    @Column(name="picture_url", length=2000)
    @JsonProperty("picture_url")
    private String pictureUrl;

    @ManyToMany(mappedBy="characters")
    List<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Character() {
    }

    public Character(String fullName, String gender, String alias, String pictureUrl) {

        this.fullName = fullName;
        this.gender = gender;
        this.alias = alias;
        this.pictureUrl = pictureUrl;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
