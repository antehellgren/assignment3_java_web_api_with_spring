package com.experis.moviecharacters.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a Movie entity.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length=500)
    private String title;
    private String genre;

    @Column(name="release_year", length=4)
    @JsonProperty("release_year")
    private String releaseYear;

    @Column(length=80)
    private String director;

    @Column(name="picture_url", length=2000)
    @JsonProperty("picture_url")
    private String pictureUrl;

    @Column(name="trailer_url", length=2000)
    @JsonProperty("trailer_url")
    private String trailerUrl;

    @ManyToOne
    @JoinColumn(name="franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchise() {
        if (franchise != null)
            return "/api/v1/franchises/" + franchise.getId();
        else
            return null;
    }
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    public List<Character> characters;

    @JsonGetter("characters")
    public List<String> getAllCharacters() {
        return characters.stream()
                .map(c -> {
                    return "/api/v1/characters/" + c.getId();
                }).collect(Collectors.toList());
    }

    public Movie() {
    }

    public Movie( String title, String genre, String releaseYear, String director, String pictureUrl, String trailerUrl) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureUrl = pictureUrl;
        this.trailerUrl = trailerUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }
}
