package com.experis.moviecharacters.databaseSeeder;

import com.experis.moviecharacters.model.Character;
import com.experis.moviecharacters.model.Franchise;
import com.experis.moviecharacters.model.Movie;
import com.experis.moviecharacters.repository.CharacterRepository;
import com.experis.moviecharacters.repository.FranchiseRepository;
import com.experis.moviecharacters.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Seeder component for initializing a postgres database with dummy data.
 * Runs automatically on program start
 *  - Adds 3 movies, 7 characters and 1 franchise.
 * @author Ante Hellgren, Marcus Cvjeticanin
 * @version 1.0
 */
@Component
public class Seeder implements ApplicationRunner {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;


    public void seedTables() {

        Character bale = new Character("Christian Bale", "Male", "Bale", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character heath = new Character("Heath Ledger", "Male", "Ledger", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character aaron = new Character("Aaron Eckhart", "Male", "Eckhart", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character michael = new Character("Michael Kane", "Male", "Kane", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character ken = new Character("Ken Watanabe", "Male", "Watanabe", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character tom = new Character("Tom Hardy", "Male", "Hardy", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character anne = new Character("Anne Hathaway", "Female", "Hathaway", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");


        Character char1Repo = characterRepository.save(bale);
        Character char2Repo = characterRepository.save(heath);
        Character char3Repo = characterRepository.save(aaron);
        Character char4Repo = characterRepository.save(michael);
        Character char5Repo = characterRepository.save(ken);
        Character char6Repo = characterRepository.save(tom);
        Character char7Repo = characterRepository.save(anne);

        var charListRepoOne = new ArrayList<Character>();
        charListRepoOne.add(char1Repo);
        charListRepoOne.add(char2Repo);
        charListRepoOne.add(char3Repo);

        var charListRepoTwo = new ArrayList<Character>();
        charListRepoTwo.add(char1Repo);
        charListRepoTwo.add(char4Repo);
        charListRepoTwo.add(char5Repo);

        var charListRepoThree = new ArrayList<Character>();
        charListRepoThree.add(char1Repo);
        charListRepoThree.add(char6Repo);
        charListRepoThree.add(char7Repo);

        Franchise franchise1 = new Franchise( "Batman", "Batman series, very epic");
        var franchiseRepo = franchiseRepository.save(franchise1);

        Movie movie1 = new Movie("Batman: The Dark Knight", "Action/Crime/Drama", "2008", "Cristopher Nolan", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i", "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi");
        Movie movie2 = new Movie("Batman: Begins", "Action/Adventure", "2005", "Cristopher Nolan", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i", "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi");
        Movie movie3 = new Movie("Batman: The Dark Knight Rises", "Action/Adventure", "2012", "Cristopher Nolan", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i", "https://www.imdb.com/video/vi324468761?playlistId=tt0468569&ref_=tt_ov_vi");

        movie1.setCharacters(charListRepoOne);
        movie1.setFranchise(franchiseRepo);
        movieRepository.save(movie1);

        movie2.setCharacters(charListRepoTwo);
        movie2.setFranchise(franchiseRepo);
        movieRepository.save(movie2);

        movie3.setCharacters(charListRepoThree);
        movie3.setFranchise(franchiseRepo);
        movieRepository.save(movie3);
    }


    @Override
    public void run(ApplicationArguments args) {
        if(
            movieRepository.count() == 0 &&
            characterRepository.count() == 0 &&
            franchiseRepository.count() == 0
        ) {
            seedTables();
        }
    }
}
