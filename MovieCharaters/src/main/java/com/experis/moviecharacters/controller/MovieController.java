package com.experis.moviecharacters.controller;

import com.experis.moviecharacters.model.Character;
import com.experis.moviecharacters.model.Movie;
import com.experis.moviecharacters.repository.CharacterRepository;
import com.experis.moviecharacters.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Character Movie Controller.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    private HttpStatus httpStatus;

    /**
     * Get all movies.
     *
     * @return ResponseEntity<List<Movie>>
     */
    @GetMapping
    public ResponseEntity<List<Movie>> listMovies() {
        List<Movie> returnMovies = movieRepository.findAll();
        return new ResponseEntity<>(returnMovies, HttpStatus.OK);
    }

    /**
     * Get a movie based on id.
     *
     * @param id : Long
     * @return ResponseEntity<Movie>
     */
    @GetMapping("{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id) {
        Movie returnMovie = new Movie();

        if (movieRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, httpStatus);
    }

    /**
     * Add a Movie.
     *
     * @param movie : Movie
     * @return ResponseEntity<Franchise>
     */
    @PostMapping
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Update a Movie.
     *
     * @param id : Long
     * @param newMovieData : Movie
     * @return ResponseEntity<Movie>
     */
    @PutMapping("{id}")
    public ResponseEntity<Movie> updateMovieById(@PathVariable Long id, @RequestBody Movie newMovieData) {

        if (!movieRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Movie movieReference = movieRepository.findById(id).get();
        movieRepository.save(newMovieData);

        return new ResponseEntity<>(movieReference, HttpStatus.OK);
    }

    /**
     * Delete a Movie.
     *
     * @param id : Long
     * @return ResponseEntity<Movie>
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Movie> deleteMovieById(@PathVariable Long id) {

        if (movieRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            Movie movie = movieRepository.findById(id).get();
            movieRepository.delete(movie);
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }

    /**
     * Get Characters in a Movie.
     *
     * @param id : Long
     * @return ResponseEntity<List<Character>>
     */
    @GetMapping("{id}/characters")
    public ResponseEntity<List<Character>> listCharactersByMovieId(@PathVariable Long id) {
        List<Character> returnCharacters = null;

        if (movieRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            Movie movie = movieRepository.findById(id).get();
            returnCharacters = movie.getCharacters();
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacters, httpStatus);
    }

    /**
     * Update Characters in a Movie.
     *
     * @param id : Long
     * @param characterIds : List<Long>
     * @return ResponseEntity<Movie>
     */
    @PutMapping("{id}/characters")
    public ResponseEntity<Movie> updateCharactersByMovieId(@PathVariable Long id, @RequestBody List<Long> characterIds) {
        if (!movieRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        ArrayList<Character> characters = new ArrayList<>();
        Movie movie = movieRepository.findById(id).get();

        for (Long characterId : characterIds) {
            if (characterRepository.existsById(characterId))
                characters.add(characterRepository.getById(characterId));
        }
        movie.setCharacters(characters);
        movieRepository.save(movie);
        return new ResponseEntity<>(movie, HttpStatus.OK);
    }
}
