package com.experis.moviecharacters.controller;

import com.experis.moviecharacters.model.Character;
import com.experis.moviecharacters.model.Franchise;
import com.experis.moviecharacters.model.Movie;
import com.experis.moviecharacters.repository.FranchiseRepository;
import com.experis.moviecharacters.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Franchise Rest Controller.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/franchises")
public class FranchiseController {
    @Autowired
    public FranchiseRepository franchiseRepository;

    @Autowired
    public MovieRepository movieRepository;
    private HttpStatus httpStatus;

    /**
     * Get all franchises.
     *
     * @return ResponseEntity<List<Franchise>>
     */
    @GetMapping
    public ResponseEntity<List<Franchise>> listFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }

    /**
     * Get a franchise based on id.
     *
     * @param id : Long
     * @return ResponseEntity<Franchise>
     */
    @GetMapping("{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();

        if (franchiseRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, httpStatus);
    }

    /**
     * Add a Franchise.
     *
     * @param franchise : Franchise
     * @return ResponseEntity<Franchise>
     */
    @PostMapping
    public ResponseEntity<Franchise> createFranchise(@RequestBody Franchise franchise) {
        Franchise returnFranchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(returnFranchise, HttpStatus.CREATED);
    }

    /**
     * Update a Franchise.
     *
     * @param id : Long
     * @param franchise : Franchise
     * @return ResponseEntity<Franchise>
     */
    @PutMapping("{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) {
        if (!franchiseRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Franchise returnFranchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(returnFranchise, HttpStatus.OK);
    }

    /**
     * Delete a Franchise.
     *
     * @param id : Long
     * @return ResponseEntity<Franchise>
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable Long id) {
        if(franchiseRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            franchiseRepository.deleteById(id);
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }

    /**
     * Get movies in a Franchise.
     *
     * @param id : Long
     * @return ResponseEntity<List<Movie>>
     */
    @GetMapping("{id}/movies")
    public ResponseEntity<List<Movie>> listMoviesByFranchiseId(@PathVariable Long id) {
        List<Movie> returnMovies = new ArrayList<>();

        if (franchiseRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            Franchise franchise = franchiseRepository.findById(id).get();
            returnMovies = franchise.getMovies();
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovies, httpStatus);
    }

    /**
     * Get characters in a Franchise.
     *
     * @param id : Long
     * @return ResponseEntity<List<Character>>
     */
    @GetMapping("{id}/characters")
    public ResponseEntity<LinkedHashSet<Character>> listCharactersByFranchiseId(@PathVariable Long id) {
        LinkedHashSet<Character> returnCharacters = new LinkedHashSet<>();

        if (franchiseRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            List<Movie> movies = franchiseRepository.findById(id).get().getMovies();

            for (Movie m : movies)
                returnCharacters.addAll(m.getCharacters());
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacters, httpStatus);
    }

    /**
     * Update movies in a Franchise.
     *
     * @param id : Long
     * @param movieIds : List<Long>
     * @return ResponseEntity<Franchise>
     */
    @PutMapping("{id}/movies")
    public ResponseEntity<Franchise> updateMoviesByFranchiseId(@PathVariable Long id, @RequestBody List<Long> movieIds) {

        if (!franchiseRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        httpStatus = HttpStatus.OK;
        Franchise returnFranchise = franchiseRepository.findById(id).get();

        ArrayList<Movie> movies = new ArrayList<>();
        for (Long movieId : movieIds) {
            if (movieRepository.existsById(movieId)) {
                Movie movie = movieRepository.findById(movieId).get();
                movies.add(movie);
                movie.setFranchise(returnFranchise);
                movieRepository.save(movie);
            }
        }

        returnFranchise.setMovies(movies);
        franchiseRepository.save(returnFranchise);

        return new ResponseEntity<>(returnFranchise, httpStatus);
    }
}
