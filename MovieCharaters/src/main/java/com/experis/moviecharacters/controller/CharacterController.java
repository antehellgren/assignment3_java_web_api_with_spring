package com.experis.moviecharacters.controller;

import com.experis.moviecharacters.model.Character;
import com.experis.moviecharacters.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Character Rest Controller.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;
    private HttpStatus httpStatus;

    /**
     * Get all Characters.
     *
     * @return ResponseEntity<List<Character>>
     */
    @GetMapping
    public ResponseEntity<List<Character>> listCharacters() {
        List<Character> returnCharacters = characterRepository.findAll();
        return new ResponseEntity<>(returnCharacters, HttpStatus.OK);
    }

    /**
     * Get a Character based on id.
     *
     * @param id : Long
     * @return ResponseEntity<Character>
     */
    @GetMapping("{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable Long id) {
        Character returnCharacter = new Character();

        if (characterRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, httpStatus);
    }

    /**
     * Add a Character.
     *
     * @param character : Character
     * @return ResponseEntity<Character>
     */
    @PostMapping
    public ResponseEntity<Character> createCharacter(@RequestBody Character character) {
        Character returnCharacter = characterRepository.save(character);
        return new ResponseEntity<>(returnCharacter, HttpStatus.CREATED);
    }

    /**
     * Update a Character.
     *
     * @param id : Long
     * @param character : Character
     * @return ResponseEntity<Character>
     */
    @PutMapping("{id}")
    public ResponseEntity<Character> updateCharacterById(@PathVariable Long id, @RequestBody Character character) {
        Character returnCharacter = new Character();

        if (!characterRepository.existsById(id))
            return new ResponseEntity<>(returnCharacter, HttpStatus.BAD_REQUEST);

        returnCharacter = characterRepository.save(character);
        return new ResponseEntity<>(returnCharacter, HttpStatus.OK);
    }

    /**
     * Delete a Character.
     *
     * @param id : Long
     * @return ResponseEntity<Character>
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Character> deleteCharacterById(@PathVariable Long id) {
        if (characterRepository.existsById(id)) {
            httpStatus = HttpStatus.OK;
            characterRepository.deleteById(id);
        } else {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(httpStatus);
    }
}
