package com.experis.moviecharacters.repository;

import com.experis.moviecharacters.model.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Franchise Jpa Repository.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
