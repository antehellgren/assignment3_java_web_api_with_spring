package com.experis.moviecharacters.repository;

import com.experis.moviecharacters.model.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Character Jpa Repository.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
}
