package com.experis.moviecharacters.repository;

import com.experis.moviecharacters.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Movie Jpa Repository.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
