package com.experis.moviecharacters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Starting point of Spring Boot Application.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
@SpringBootApplication
public class MovieCharactersApplication {
    public static void main(String[] args) {
        SpringApplication.run(MovieCharactersApplication.class, args);
    }
}