package com.experis.moviecharacters;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Represents the ServletInitializer.
 *
 * @author Marcus Cvjeticanin & Ante Hellgren
 * @version 1.0
 */
public class ServletInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MovieCharactersApplication.class);
    }
}
