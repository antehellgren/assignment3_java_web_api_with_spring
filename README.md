# Experis Java Assignment 3

This is the Java Assignment 3 in the Experis Academy program.

## Technologies

This Spring Boot application are using:

- JDK 16
- Spring Boot 2.5.2
- Hibernate
- PostgreSQL
- Gradle
- Swagger UI

## E/R Diagram

![E/R Diagram](er-diagram.png "E/R Diagram")

## Database Schema

![Database Schema](database-schema.png "Database Schema")

## Class Diagram

![Class Diagram](class-diagram.png "Class Diagram")

## API Endpoints

The available REST API Endpoints can be seen below. To check out the responses of using the API you can import the file **MovieCharacters.postman_collection.json** to Postman.

### CharacterController

#### GET
    
    /api/v1/characters
    /api/v1/characters/{id}

#### POST

    /api/v1/characters

#### PUT

    /api/v1/characters/{id}

#### DELETE

    /api/v1/characters/{id}

### FranchiseController

#### GET

    /api/v1/franchises
    /api/v1/franchises/{id}
    /api/v1/franchises/{id}/movies
    /api/v1/franchises/{id}/characters

#### POST

    /api/v1/franchises

#### PUT

    /api/v1/franchises/{id}

#### DELETE

    /api/v1/franchises/{id}

### MovieController

#### GET

    /api/v1/movies
    /api/v1/movies/{id}
    /api/v1/movies/{id}/characters

#### POST

    /api/v1/movies

#### PUT

    /api/v1/movies/{id}
    /api/v1/movies/{id}/characters

#### DELETE

    /api/v1/movies/{id}

## Team
Marcus Cvjeticanin - [@mjovanc](https://github.com/mjovanc)
\
Ante Hellgren - [@anthel](https://github.com/anthel)

